package com.jmapelli.back01.service.impl;

import com.jmapelli.back01.dto.Customer;
import com.jmapelli.back01.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final Map<String, Customer> customers = new HashMap<>();

    @Override
    public List<Customer> listCustomer() {
        return new ArrayList<>(this.customers.values());
    }

    @Override
    public void createCustomer(Customer customer) {
        this.customers.put(customer.dni, customer);
    }

    @Override
    public Customer getCustomer(String dni) {
        return this.customers.get(dni);
    }

    @Override
    public void updateCustomer(String dni, Customer customer) {
        customer.dni = dni;
        this.customers.replace(dni, customer);
    }

    @Override
    public void patchCustomer(String dni, Customer source) {
        Customer target = this.getCustomer(dni);

        if (Objects.nonNull(source.fullname)) {
            target.fullname = source.fullname;
        }

        if (Objects.nonNull(source.age)) {
            target.age = source.age;
        }

        if (Objects.nonNull(source.birthdate)) {
            target.birthdate = source.birthdate;
        }

        if (Objects.nonNull(source.phone)) {
            target.phone = source.phone;
        }

        if (Objects.nonNull(source.email)) {
            target.email = source.email;
        }

        if (Objects.nonNull(source.address)) {
            target.address = source.address;
        }

        this.updateCustomer(dni, target);
    }

    @Override
    public void deleteCustomer(String dni) {
        this.customers.remove(dni);
    }

}
