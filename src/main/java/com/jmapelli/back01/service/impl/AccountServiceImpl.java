package com.jmapelli.back01.service.impl;

import com.jmapelli.back01.dto.Account;
import com.jmapelli.back01.service.AccountService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class AccountServiceImpl implements AccountService {

    private final Map<String, Map<String, Account>> accounts = new HashMap<>();

    @Override
    public List<Account> listAccounts(String dni) {
        Map<String, Account> customerAccounts = this.accounts.get(dni);

        if (Objects.isNull(customerAccounts)) {
            return Collections.emptyList();
        }

        return new ArrayList<>(customerAccounts.values());
    }

    @Override
    public void createAccount(String dni, Account account) {
        Map<String, Account> customerAccounts = this.accounts.get(dni);

        if (Objects.isNull(customerAccounts)) {
            customerAccounts = new HashMap<>();
        }

        customerAccounts.put(account.number, account);

        this.accounts.put(dni, customerAccounts);
    }

    @Override
    public Account getAccount(String dni, String accountNumber) {
        Map<String, Account> customerAccounts = this.accounts.get(dni);

        if (Objects.isNull(customerAccounts)) {
            return null;
        }

        return customerAccounts.get(accountNumber);
    }

    @Override
    public void updateAccount(String dni, String accountNumber, Account account) {
        Map<String, Account> customerAccounts = this.accounts.get(dni);
        customerAccounts.replace(accountNumber, account);

        account.number = accountNumber;

        this.accounts.put(dni, customerAccounts);
    }

    @Override
    public void patchAccount(String dni, String accountNumber, Account source) {
        Map<String, Account> customerAccounts = this.accounts.get(dni);
        Account target = customerAccounts.get(accountNumber);

        if (Objects.nonNull(source.status)) {
            target.status = source.status;
        }

        if (Objects.nonNull(source.balance)) {
            target.balance = source.balance;
        }

        customerAccounts.replace(accountNumber, target);

        this.accounts.replace(dni, customerAccounts);
    }

    @Override
    public void deleteAccount(String dni, String accountNumber) {
        Map<String, Account> customerAccounts = this.accounts.get(dni);
        customerAccounts.remove(accountNumber);

        this.accounts.replace(dni, customerAccounts);
    }


}
