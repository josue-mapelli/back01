package com.jmapelli.back01.controller;

import com.jmapelli.back01.dto.Account;
import com.jmapelli.back01.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/customers/{dni}/accounts/{accountNumber}")
public class AccountNumberController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public ResponseEntity getAccount(@PathVariable String dni,
                                     @PathVariable String accountNumber) {
        Account account = this.accountService.getAccount(dni, accountNumber);

        if (Objects.isNull(account)) {
            return new ResponseEntity<>("Account not found.", HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(account);
    }

    @PutMapping
    public ResponseEntity putAccount(@PathVariable String dni,
                                     @PathVariable String accountNumber,
                                     @RequestBody final Account account) {
        Account target = this.accountService.getAccount(dni, accountNumber);

        if (Objects.isNull(target)) {
            return new ResponseEntity<>("Account not found.", HttpStatus.NOT_FOUND);
        }

        this.accountService.updateAccount(dni, accountNumber, account);

        return new ResponseEntity<>("Account updated successfully!", HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity patchAccount(@PathVariable String dni,
                                       @PathVariable String accountNumber,
                                       @RequestBody final Account account) {
        Account target = this.accountService.getAccount(dni, accountNumber);

        if (Objects.isNull(target)) {
            return new ResponseEntity<>("Account not found.", HttpStatus.NOT_FOUND);
        }

        this.accountService.patchAccount(dni, accountNumber, account);

        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity deleteAccount(@PathVariable String dni,
                                        @PathVariable final String accountNumber) {
        Account account = this.accountService.getAccount(dni, accountNumber);

        if (Objects.isNull(account)) {
            return new ResponseEntity<>("Account not found.", HttpStatus.NOT_FOUND);
        }

        this.accountService.deleteAccount(dni, accountNumber);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
