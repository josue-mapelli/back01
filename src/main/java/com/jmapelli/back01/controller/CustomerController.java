package com.jmapelli.back01.controller;

import com.jmapelli.back01.dto.Customer;
import com.jmapelli.back01.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<Customer>> listAccounts() {
        List<Customer> customers = this.customerService.listCustomer();
        return ResponseEntity.ok(customers);
    }

    @PostMapping
    public ResponseEntity<String> createAccount(@RequestBody final Customer customer) {
        this.customerService.createCustomer(customer);
        return new ResponseEntity<>("Customer created successfully!", HttpStatus.CREATED);
    }

}
