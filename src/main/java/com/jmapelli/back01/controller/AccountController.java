package com.jmapelli.back01.controller;

import com.jmapelli.back01.dto.Account;
import com.jmapelli.back01.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers/{dni}/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public ResponseEntity<List<Account>> listAccounts(@PathVariable String dni) {
        List<Account> accounts = this.accountService.listAccounts(dni);
        return ResponseEntity.ok(accounts);
    }

    @PostMapping
    public ResponseEntity<String> createAccount(@PathVariable String dni,
                                                @RequestBody final Account account) {
        this.accountService.createAccount(dni, account);
        return new ResponseEntity<>("Account created successfully!", HttpStatus.CREATED);
    }


}
