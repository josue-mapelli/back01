package com.jmapelli.back01.controller;

import com.jmapelli.back01.dto.Customer;
import com.jmapelli.back01.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/customers/{dni}")
public class CustomerDniController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public ResponseEntity getCustomer(@PathVariable String dni) {
        Customer customer = this.customerService.getCustomer(dni);

        if (Objects.isNull(customer)) {
            return new ResponseEntity<>("Customer not found.", HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(customer);
    }

    @PutMapping
    public ResponseEntity putCustomer(@PathVariable String dni,
                                      @RequestBody final Customer source) {
        Customer target = this.customerService.getCustomer(dni);

        if (Objects.isNull(target)) {
            return new ResponseEntity<>("Customer not found.", HttpStatus.NOT_FOUND);
        }

        this.customerService.updateCustomer(dni, source);

        return new ResponseEntity<>("Customer updated successfully!", HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity patchCustomer(@PathVariable String dni,
                                        @RequestBody final Customer source) {
        Customer target = this.customerService.getCustomer(dni);

        if (Objects.isNull(target)) {
            return new ResponseEntity<>("Customer not found.", HttpStatus.NOT_FOUND);
        }

        this.customerService.patchCustomer(dni, source);

        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity deleteCustomer(@PathVariable String dni) {
        Customer account = this.customerService.getCustomer(dni);

        if (Objects.isNull(account)) {
            return new ResponseEntity<>("Customer not found.", HttpStatus.NOT_FOUND);
        }

        this.customerService.deleteCustomer(dni);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
